import json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .utils import create_vector, get_default_skin, create_inventory
from .serializer import serialize_player

from .models import Inventory, Player, PlayerSerializer

def index(request):
    return HttpResponse("Hello, world. You're at the API index.")

def get_player(request, pk):
    if request.method != 'GET':
        return JsonResponse({"error": "GET only. This request was '" + str(request.method) + "'."}, status=400)
    # Get the player instance
    try:
        player = Player.objects.get(pk=pk)
    except Player.DoesNotExist:
        return JsonResponse({"error": "No player with ID '" + str(pk) + "'."}, status=404)
    # Return the serialized data
    return JsonResponse(serialize_player(player))

def create_new_player(request, username):
    #if request.method != 'POST':
    #    return JsonResponse({"error": "POST only. This request was '" + str(request.method) + "'."}, status=400)
    if Player.objects.filter(username=username).count() > 0:
        return JsonResponse({"error": "A player with username '" + str(username) + "' already exists !."}, status=400)

    player = Player(
        username=username,
        last_pos=create_vector(0, 0, 0),
        inventory=create_inventory(10), #TODO savoir quelle taille d'inventaire avoir
        skin=get_default_skin(),
    )
    player.save()
    return JsonResponse(serialize_player(player), safe=False)
