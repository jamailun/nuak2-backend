from django.contrib import admin

from .models import Skin, Player, Inventory, SlotedItem, Item, Color, Vector

admin.site.register(Player)
admin.site.register(Skin)

admin.site.register(Inventory)
admin.site.register(SlotedItem)
admin.site.register(Item)

admin.site.register(Color)
admin.site.register(Vector)