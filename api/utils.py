from .models import Vector, Color, Skin, Item, SlotedItem, Inventory, DEFAULT_SKIN_NAME

def create_vector(x = 0, y = 0, z = 0):
    vector = Vector(x=x, y=y, z=z)
    vector.save()
    return vector
def create_color(r = 0, g = 0, b = 0):
    color = Color(r=r, g=g, b=b)
    color.save()
    return color

def get_default_skin():
    if Skin.objects.filter(name=DEFAULT_SKIN_NAME).count() > 0:
        return Skin.objects.filter(name=DEFAULT_SKIN_NAME).first()
    skin = Skin(
        name=DEFAULT_SKIN_NAME,
        color_hand=create_color(0, 0, 255),
        color_body=create_color(0, 0, 255),
    )
    skin.save()
    return skin

# Inventory ahah

def get_or_create_item(id):
    try:
        return Item.objects.get(id=id)
    except Item.DoesNotExist:
        item = Item(
            id=id,
            # modifiers, plus tard ?
        )
        item.save()
        return item

def create_sloted_item(inventory, item_id, slot_id, amount = 1):
    sloted_item = SlotedItem(
        inventory = inventory,
        slot = slot_id,
        amount = amount,
        item = get_or_create_item(item_id)
    )
    sloted_item.save()
    return sloted_item

def create_inventory(size):
    inventory = Inventory(
        size = size,
    )
    inventory.save()
    return inventory