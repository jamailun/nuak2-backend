from django.db import models
from rest_framework import serializers
from django.core.validators import MaxValueValidator, MinValueValidator

class Skin(models.Model):
    name = models.CharField(max_length=32)
    # Color hands
    color_hand_r = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])
    color_hand_g = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])
    color_hand_b = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])
    # Color body
    color_body_r = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])
    color_body_g = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])
    color_body_b = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])

    
    def __str__(self):
        return "Skin[" + self.name + "]"

class SkinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skin
        fields = '__all__'