from django.db import models
from rest_framework import serializers

from .Skin import Skin, SkinSerializer
from .Inventory import Inventory, InventorySerializer

class Player(models.Model):
    username = models.CharField(max_length=16)
    skin = models.ForeignKey(Skin, on_delete=models.DO_NOTHING)
    inventory = models.ForeignKey(Inventory, on_delete=models.CASCADE)

    def __str__(self):
        return "Player '" + self.username + "'."

class PlayerSerializer(serializers.ModelSerializer):
    skin = SkinSerializer(read_only=True)
    inventory = InventorySerializer(read_only=True)
    class Meta:
        model = Player
        fields = '__all__'