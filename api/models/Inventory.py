from django.db import models
from rest_framework import serializers
from django.core.validators import MinValueValidator

class Item(models.Model):
    id = models.IntegerField(primary_key=True)
    #TODO les modifiers

    def __str__(self):
        return "Item{id=" + str(self.id) + "}"

class Inventory(models.Model):
    size = models.IntegerField()

    @property
    def owner(self):
        return self.player_set.first()

    def __str__(self):
        return "Inventory{size=" + str(self.size) + ", owner='" + self.owner.username + "'}"


class SlotedItem(models.Model):
    inventory = models.ForeignKey(Inventory, on_delete=models.DO_NOTHING)
    slot = models.SmallIntegerField(default=0, validators=[MinValueValidator(0)])
    amount = models.SmallIntegerField(default=1, validators=[MinValueValidator(1)])
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    def __str__(self):
        return "SlotedItem{slot=" + str(self.slot) + ", amount=" + str(self.amount) + ", item=" + str(self.item) + "}"


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'

class SlotedItemSerializer(serializers.ModelSerializer):
    item = ItemSerializer(read_only=True)
    class Meta:
        model = SlotedItem
        fields = '__all__'

class InventorySerializer(serializers.ModelSerializer):
    phases = SlotedItemSerializer(source='sloteditem_set', many=True, read_only=True)
    class Meta:
        model = Inventory
        fields = '__all__'