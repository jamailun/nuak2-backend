from .models import Player, Inventory, Skin, SlotedItem, Item, Color, Vector

def serialize_vector(vector):
    if vector == None:
        return None
    return {
        'x': vector.x,
        'y': vector.y,
        'z': vector.z
    }
    
def serialize_color(color):
    if color == None:
        return None
    return {
        'x': color.r,
        'y': color.g,
        'z': color.b
    }
    
def serialize_skin(skin):
    if skin == None:
        return None
    return {
        'name': skin.name,
        'color_body': serialize_color(skin.color_body),
        'color_hand': serialize_color(skin.color_hand),
    }

def serialize_item(item):
    if item == None:
        return None
    return {
        'id': item.id
    }
    
def serialize_sloted_item(sloted_item):
    if sloted_item == None:
        return None
    return {
        'slot': sloted_item.slot,
        'amount': sloted_item.amount,
        'item': serialize_item(sloted_item.item)
    }
    
def serialize_inventory(inventory):
    if inventory == None:
        return None
    data = {
        'size': inventory.size,
    }
    items = []
    for slotitem in inventory.sloteditem_set.all():
        items.push(serialize_sloted_item(slotitem))
    data['items'] = items
    return data

def serialize_player(player):
    if player == None:
        return None
    return {
        'username': player.username,
        'pk': player.pk,
        'las_pos': serialize_vector(player.last_pos),
        'skin': serialize_skin(player.skin),
        'inventory': serialize_inventory(player.inventory),
    }
