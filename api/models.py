from django.db import models
from rest_framework import serializers
from django.core.validators import MaxValueValidator, MinValueValidator

DEFAULT_ID = 1

# =================================================================================================================== #
# ==============================                       VECTOR                           ============================== #
# =================================================================================================================== #

class Vector(models.Model):
    x = models.FloatField()
    y = models.FloatField()
    z = models.FloatField()
    def __str__(self):
        return "Vector(" + str(self.x) + ", " + str(self.y) + ", " + str(self.z) + ")"

class VectorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vector
        fields = '__all__'

# =================================================================================================================== #
# ==============================                       COLOR                           ============================== #
# =================================================================================================================== #

class Color(models.Model):
    name = models.CharField(null=True, max_length=8)
    r = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])
    g = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])
    b = models.SmallIntegerField(default=128, validators=[MinValueValidator(0), MaxValueValidator(255)])
    def __str__(self):
        if self.name == None:
            return "Color(" + str(self.r) + ", " + str(self.g) + ", " + str(self.b) + ")"
        return "Color_" + self.name

class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = '__all__'

# =================================================================================================================== #
# ==============================                        SKIN                           ============================== #
# =================================================================================================================== #

DEFAULT_SKIN_NAME = "default"

class Skin(models.Model):
    name = models.CharField(max_length=32)

    color_hand = models.ForeignKey(Color, default=DEFAULT_ID, on_delete=models.CASCADE, related_name="skin_color_hand")
    color_body = models.ForeignKey(Color, default=DEFAULT_ID, on_delete=models.CASCADE, related_name="skin_color_body")

    def __str__(self):
        return "Skin[" + self.name + "]"

class SkinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skin
        fields = '__all__'
        
# =================================================================================================================== #
# ==============================                   INVENTORY-STUFF                     ============================== #
# =================================================================================================================== #

class Item(models.Model):
    id = models.IntegerField(primary_key=True)
    #TODO les modifiers

    def __str__(self):
        return "Item{id=" + str(self.id) + "}"

class Inventory(models.Model):
    size = models.IntegerField()

    equiped_hand = models.ForeignKey(Item, null=True, on_delete=models.DO_NOTHING, related_name="inv_equiped_hand")
    equiped_offhand = models.ForeignKey(Item, null=True, on_delete=models.DO_NOTHING, related_name="inv_equiped_offhand")
    equiped_head = models.ForeignKey(Item, null=True, on_delete=models.DO_NOTHING, related_name="inv_equiped_head")
    equiped_neck = models.ForeignKey(Item, null=True, on_delete=models.DO_NOTHING, related_name="inv_equiped_neck")
    equiped_art1 = models.ForeignKey(Item, null=True, on_delete=models.DO_NOTHING, related_name="inv_equiped_art1")
    equiped_art2 = models.ForeignKey(Item, null=True, on_delete=models.DO_NOTHING, related_name="inv_equiped_art2")

    @property
    def owner(self):
        return self.player_set.first()

    def __str__(self):
        return "Inventory{size=" + str(self.size) + ", owner='" + self.owner.username + "'}"
        
    class Meta: # Affichage custom dans l'onglet d'administration
       verbose_name = 'Inventory'
       verbose_name_plural = 'Inventories'


class SlotedItem(models.Model):
    inventory = models.ForeignKey(Inventory, on_delete=models.DO_NOTHING)
    slot = models.SmallIntegerField(default=0, validators=[MinValueValidator(0)])
    amount = models.SmallIntegerField(default=1, validators=[MinValueValidator(1)])
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    def __str__(self):
        return "SlotedItem{slot=" + str(self.slot) + ", amount=" + str(self.amount) + ", item=" + str(self.item) + "}"

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'

class SlotedItemSerializer(serializers.ModelSerializer):
    item = ItemSerializer(read_only=True)
    class Meta:
        model = SlotedItem
        exclude = ('inventory', )

class InventorySerializer(serializers.ModelSerializer):
    items = SlotedItemSerializer(source='sloteditem_set', many=True, read_only=True)
    class Meta:
        model = Inventory
        fields = '__all__'

# =================================================================================================================== #
# ==============================                       PLAYER                          ============================== #
# =================================================================================================================== #


class Player(models.Model):
    username = models.CharField(max_length=16)
    last_pos = models.ForeignKey(Vector, null=True, on_delete=models.CASCADE, related_name="player_last_pos")

    skin = models.ForeignKey(Skin, on_delete=models.DO_NOTHING)
    inventory = models.ForeignKey(Inventory, on_delete=models.CASCADE)

    def __str__(self):
        return "Player '" + self.username + "'."

class PlayerSerializer(serializers.ModelSerializer):
    skin = SkinSerializer(read_only=True)
    inventory = InventorySerializer(read_only=True)
    class Meta:
        model = Player
        fields = '__all__'