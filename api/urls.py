from django.urls import path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('', views.index, name='index'),
    path('player/get/<int:pk>/', views.get_player),
    path('player/create/<username>/', views.create_new_player),
    #path('etudes/', include('flower_app.controllers.etudes.urls')),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]